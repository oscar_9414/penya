@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Lista de productos</h1> <br>
                    <a href="/products/create" class="btn btn-primary btn-block">Crear nuevo producto</a>
                </div>

                    <div class="card-body">
                        @if(count($products) != 0)
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <td>Nombre</td>
                                    <td>Precio</td>
                                    <td>Categoría</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($products as $product)
                                @foreach ($categorias as $categoria)
                                @if($product->cathegory_id == $categoria->id)
                                <tr>
                                    <td> {{ $product->name}} </td>
                                    <td> {{$product->price}} </td>
                                    <td> {{ $categoria->name}} </td>
                                    <td>
                                        <a href="/products/{{$product->id}}/edit" class="btn btn-primary">Edit</a>
                                        <a href="/products/{{$product->id}}" class="btn btn-primary">Ver</a>
                                        {{-- Basket --}}
                                        <a href="/basket/{{ $product->id }}" class="btn btn-primary">Añadir al carrito</a>
                                        {{-- Basket --}}
                                        <form method="post" action="/products/{{ $product->id }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="submit" value="borrar">
                                        </form>
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                                @empty
                                -No hay productos
                                @endforelse
                            </tbody>
                        </table>
                        @else
                        <div class="alert alert-danger">
                            <h1>No hay productos!!!</h1>
                      </div>

                      @endif
                      {{ $products->render() }}
                  </div>
              </div>
          </div>
      </div>
  </div>
  @endsection
