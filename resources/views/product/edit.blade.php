@extends('layouts.app')

@section('title', 'Productos')

@section('content')
<h1>Editar producto</h1>

<form method="post" action="/products/{{ $product->id }}">
    {{ csrf_field() }}

    <input type="hidden" name="_method" value="PUT">

    <label>Nombre</label>
    <input type="text" name="name"
    value="{{ old('name') ? old('name') : $product->name }}">
    <div class="alert alert-danger">
        {{ $errors->first('name') }}
    </div>
    <br>

    <br>

    <label>Precio</label>
    <input type="text" name="price"
    value="{{ old('price') ? old('price') : $product->price }}">
    <div class="alert alert-danger">
        {{ $errors->first('price') }}
    </div>
    <br>
    <label> Categoría: </label>
    <select name="cathegory_id">
        @foreach ($categorias as $categoria)
        <option value="{{ $categoria->id }}"
            {{ old('cathegory_id') == $categoria?
            'selected="selected"' :
            ''
        }}>{{ $categoria->name }}
    </option>
    @endforeach
    <div class="alert alert-danger">
        {{ $errors->first('cathegory_id') }}
    </div>
</select>
<br>

<input type="submit" value="Terminar">
</form>
<br>
<a href="/products" class="btn btn-primary"> Volver </a>
@endsection
