@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de usuarios</h1>
      <div class="alert">
        <a href="/users/create" class="btn btn-primary">Nuevo</a>
    </div>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <td>Nombre</td>
                <td>Email</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>


            @forelse ($users as $user)
            <tr>
                <td> {{ $user->name }} </td>
                <td> {{ $user->email }} </td>
                <td> {{ $user->role->name }} </td>
                <td>
                    <a class="btn btn-primary" role="button" href="/users/{{ $user->id }}/edit">Editar</a>

                    <form method="post" action="/users/{{ $user->id }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        @can ('delete' , $user)
                            <input type="submit" value="borrar" class="btn btn-primary">
                        @endcan
                    </form>
                    <a class="btn btn-primary" href="/users/{{$user->id}}">Ver</a>
                    <a class="btn btn-success" href="/group/{{ $user->id}}">Guardar</a>
                </td>

            </tr>

            @empty
            <li>No hay usuarios!!</li>
            @endforelse
        </tbody>
    </table>
    {{ $users->render() }}
</div>
</div>
</div>
@endsection
