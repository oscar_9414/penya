@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crear categoría<br>
                    <div class="card-body">
                        <form method="post" action="/category">
                            {{ csrf_field() }}
                            <label>Nombre:</label>
                            <input type="text" name="name" value="{{ old('name') }}">
                            <div class="alert alert-danger">
                                {{ $errors->first('name')}}
                            </div>
                    <br>
                    <input type="submit" value="Crear">
                </form>

            </div>
        </div>
    </div>
</div>
</div>
@endsection
