@extends('layouts.app')

@section('title', 'Category')

@section('content')
<h1>Editar Categoría</h1>

<form method="post" action="/category/{{ $categoria->id }}">
    {{ csrf_field() }}

    <input type="hidden" name="_method" value="PUT">

    <label>Nombre</label>
    <input type="text" name="name"
    value="{{ old('name') ? old('name') : $categoria->name }}">
    <div class="alert alert-danger">
        {{ $errors->first('name') }}
    </div>
    <br>
<br>

<input type="submit" value="Terminar">
</form>
<br>
<a href=/category class="btn btn-primary"> Volver </a>
@endsection
