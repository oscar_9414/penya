@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Lista de roles</h1> <br>
                    {{-- <a href="/products/create" class="btn btn-primary">Nuevo</a></div> --}}

                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <td>Nombre</td>
                                    <td>ID</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($roles as $rol)
                                <tr>
                                    <td>{{$rol->name}}</td>
                                    <td>{{$rol->id}}</td>

                                    <td>
                                        @can('view',$rol)
                                        <a class="btn btn-primary" href="/roles/{{$rol->id}}">ver</a>
                                        @endcan
                                    </td>
                                </tr>

                                @empty
                                -No hay Roles
                                @endforelse
                            </tbody>
                        </table>
                        {{ $roles->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
