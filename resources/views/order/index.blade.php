@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header"><h1>lista de los pedidos</h1><br>
          <a href="/products" class="btn btn-primary btn-block">Crear nuevo pedido</a>
        </div>
        <div class="card-body">
          @if(count($orders) != 0)
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <td><h4> Nº Pedido </h4></td>
                <td><h4>Estado</h4></td>
                <td><h4>Fecha de pedido</h4></td>
                <td><h4>Usuario</h4></td>
                <td><h4>Acciones</h4></td>
              </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)
              @can('view',$order)
              <tr>
                <td>{{$order->id}}</td>
                <td>
                  @if( $order->paid == 1)
                  Pagado
                  @endif

                  @if( $order->paid == 0)
                  Pendiente
                  @endif
                </td>
                <td>{{ $order->date->format('d-m-Y')}}</td>
                <td>{{ $order->user->name}}</td>
                <td>
                  <form method="post" action="/order/{{ $order->id }}">
                    <a href="/order/{{ $order->id }}" class="btn btn-primary">Ver</a>
                    <a href="/order/{{ $order->id }}/pdf" class="btn btn-primary">PDF</a>

                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    @can('delete',$order)
                    <input type="submit" value="borrar" class="btn btn-primary">
                    @endcan
                  </form>
                </td>
              </tr>
              @endcan

              @endforeach
            </tbody>
          </table>
          @else
          <div class="alert alert-danger">
            <h1>No hay pedidos!!!</h1>
          </div>

          @endif
          {{ $orders->render() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
