@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    <h1 style="text-align: center;">Datos del pedido. {{ $order->id }}</h1>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <td><strong>Nº PEdido</strong></td>
                                <td><strong>Pagado</strong></td>
                                <td><strong>Fecha de pedido</strong></td>
                                <td><strong>Usuario</strong></td>
                                @can('update',$order)
                                <td><strong>Accciones</strong></td>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>
                                    @if( $order->paid == 1)
                                    Pagado
                                    @endif

                                    @if( $order->paid == 0)
                                    Pendiente
                                    @endif
                                </td>
                                <td>{{ $order->date->format('d-m-Y') }}</td>
                                <td>{{ $order->user->name }}</td>
                                @can('update',$order)
                                <td>
                                    {{-- <a href="/order/{{ $order->id }}/edit" class="btn btn-primary">Cambiar</a> --}}
                                    <form method="post" action="/order/{{ $order->id }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PUT">
                                        <select name="paid" class=" btn btn-primary">
                                            <option value="1">Pagado</option>
                                            <option value="0">Pendiente</option>
                                        </select>
                                        <input type="submit" value="Terminar" class="btn btn-primary">
                                    </form>
                                </td>
                                @endcan
                            </tr>

                        </tbody>
                    </table>
                    <h3 style="text-align: center;">Lista de productos de este pedido</h3>
                    <table class="table table-striped table-hover">
                        <thead>
                            <td><strong>NOMBRE</strong></td>
                            <td><strong>PRECIO</strong></td>
                            <td><strong>CANTIDAD</strong></td>
                        </thead>
                        <tbody>
                           @foreach($products as $product)
                           <tr>
                            <td>{{$product->name}}</td>
                            <td>{{$product->price}}</td>
                            <td>{{$product->pivot->quantity}}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" style="text-align: center;">Total:{{ $order->total($order->id) }}</td>
                        </tr>
                    </tbody>
                </table>
                <a href="/order" class="btn btn-primary">Volver</a>
                <a href="/order/{{$order->id}}/pdf" class="btn btn-primary">Descargar pdf</a>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
