<!DOCTYPE html>
<html>
<head>
    <title>Detalle de pedido</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>

    <h1 style="text-align: center;">Datos del pedido. {{ $order->id }}</h1>
    <div class="container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td><strong>Nº PEdido</strong></td>
                    <td><strong>Fecha de pedido</strong></td>
                    <td><strong>Usuario</strong></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->date->format('d-m-Y') }}</td>
                    <td>{{ $order->user->name }}</td>
                </tr>

            </tbody>
        </table>
        <h3 style="text-align: center;">Lista de productos de este pedido</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td><strong>NOMBRE</strong></td>
                    <td><strong>PRECIO</strong></td>
                    <td><strong>CANTIDAD</strong></td>
                    <td><strong>TOTAL</strong></td>
                </tr>
            </thead>
            <tbody>
               @foreach($products as $product)
               <tr>
                <td>{{$product->name}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->pivot->quantity}}</td>
                <td>{{$product->pivot->quantity*$product->price}}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="3" style="text-align: center;">Cantidad total a pagar:</td>

                <td>{{ $order->total($order->id) }}</td>
            </tr>
        </tbody>
    </table>
</div>

</body>
</html>
