@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Lista de usuarios en session</h1> <br>
                    {{-- <a href="/products/create" class="btn btn-primary">Nuevo</a></div> --}}

                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <td>Nombre</td>
                                    <td>ID</td>
                                    <td>Role</td>
                                    <td>Cantidad</td>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->role->name}}</td>
                                    <td>{{$user->cantidad}}</td>
                                </tr>

                                @empty
                                <h1>-No hay grupos</h1>
                                @endforelse
                            </tbody>
                        </table>
                        <a href="/group/flush" class="btn btn-primary">vaciar lista</a>
                        <a href="/users" class="btn btn-primary">Volver</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
