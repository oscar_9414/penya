@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Basket list</h1> <br>
                    <a href="/products" class="btn btn-primary btn-block">Añadir productos</a>
                </div>

                    <div class="card-body">
                        @if(!$products==null)
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <td>Nombre</td>
                                    <td>Precio Unidad</td>
                                    <td>Categoría</td>
                                    <td>Cantidad</td>
                                    <td>Importe</td>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($products as $product)
                                <tr>
                                    <td>
                                        {{$product->name}}
                                    </td>
                                    <td>
                                        {{$product->price}}-€
                                    </td>
                                    <td>
                                        {{ $product->cathegory->name }}
                                    </td>
                                    <td>
                                        <a class="btn btn-success" href="/basket/{{ $product->id }}">+</a>
                                        <strong>{{$product->cantidad}}</strong>
                                        <a class="btn btn-success" href="/basket/delete/{{ $product->id }}">-</a>
                                    </td>
                                    <td>
                                        {{ $product->importe}}
                                    </td>

                                </tr>

                                @endforeach
                                @if(!$products == null)
                                <tr>
                                    <td>
                                        <a href="/basket/flush" class="btn btn-success">Vaciar Basket</a>
                                    </td>
                                    <td>
                                        <form method="post" action="/basket">
                                            {{ csrf_field() }}
                                            <input type="submit" name="" value="confirmar compra" class=" btn btn-danger">
                                        </form>
                                        {{-- <a href="/basket/store" class="btn btn-danger">Confirmar pedido</a> --}}
                                        {{-- {{ csrf_field() }} --}}
                                    </td>
                                    <td colspan="3">
                                        <div class="alert alert-success" style="float: right;">
                                            <strong>Total:{{ $total}}</strong>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @else
                        <div class="alert alert-danger">
                            <h1>No hay productos en el Basket</h1>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
