<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\User;

class Order extends Model
{
    protected $fillable = [
        'paid', 'date', 'user_id'
    ];

    protected $dates=[
        "date",
    ];

    public function total($id){ // esto es el "paid" del store una vez confirmamos la compra.

    $order = Order::findOrFail($id);
    $total = 0;
        foreach ($order->products as $product) { // this se refiere al objeto que llama, osea a $order
            $total += $product->pivot->price * $product->pivot->quantity;

        }
        return $total;
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('order_id','product_id','quantity','price');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderProduct()
    {
        return $this->belongsToMany(Product::class)->withPivot('order_id','product_id','quantity','price');
    }
    public function borrarPedido()
    {
        return $this->hasMany(Product::class);
    }
}
