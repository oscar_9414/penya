<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;
use App\User;
use PDF;
use Mail;
use Auth;

class Email extends Mailable{
    use Queueable, SerializesModels;

    public function __construct(Order $order){
        $this->order = $order;

    }

    public function build(){
        $from="administrador@adminEmail.com";
        $name="Admin";
        $user = \Auth::user();
        $products = $this->order->orderProduct;
        $pdf = PDF::loadView('order.pedido', ['order' => $this->order],['products'=>$products])->save('pedido');

        $email = $this->view('mail.mail')->with('order', $this->order)->from($from, $name)->subject('El pedido se realizó correctamente');

        $email->attachData($pdf->output(),"Pedido.pdf");

    } //build

}
