<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     protected $fillable = [
        'name', 'price', 'cathegory_id'
    ];
    public function cathegory()
    {

        return $this->belongsTo(Cathegory::class);
        //return Cathegory::findOrFail($this->cathegory_id);
    }

}
