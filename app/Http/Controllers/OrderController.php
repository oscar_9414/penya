<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Mail\Email;
use Auth;
use Mail;
use PDF;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        $orders = Order::paginate(10);
        //return $orders;
        return view('order.index',['orders'=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *ss
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function pdf($id)
    {
        /**
         * toma en cuenta que para ver los mismos
         * datos debemos hacer la misma consulta
        **/
        $order = Order::findOrFail($id);
        $products = $order->orderProduct;

        $pdf = PDF::loadView('order.pedido',['order'=>$order],['products'=>$products]);

        return $pdf->stream('pedido')->header('Content-Type','application/pdf');
    }
    public static function enviarEmail($id)
    {
        $order = Order::findOrFail($id);
        $user = Auth::user();
        Mail::to($user->email)->send(new Email($order));//nombre del email que creamos con php artisan make:mail

        return redirect("/orders/$id");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        $products = $order->orderProduct;
        // return $order->total;
        // return $products;
        // $total = 0;
        // foreach ($products as $product) {
        //     $total += $product->price*$product->pivot->quantity;
        // }
        $this->authorize('view',$order);
        return view('order.show',['order'=>$order],['products'=>$products]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        return view ('order.edit',['order'=>$order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->fill($request->all());
        $this->authorize('update',$order);
        $order->save();
        return redirect('/order/'.$order->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {

        $pedido = Order::findOrFail($id);

       // User::find(1)->groups()->detach();
        Order::find($id)->orderProduct()->detach();
        $pedido->delete();
        return back();
    }
}
