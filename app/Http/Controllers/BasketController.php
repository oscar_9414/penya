<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;
use App\Order;
use \Carbon\Carbon;
use Session;

class BasketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $basket = $request->session()->get('basket');
        if (! $basket) {
            $basket = [];
        }
        $total = 0;
        foreach ($basket as $totalFinal) {
            $total+= $totalFinal->importe;
        }
        // return Auth()->user()->role_id;
        return view('basket.index',['products'=> $basket],['total' => $total]);

    }
    public function flush(Request $request)
    {
        $request->session()->forget('basket');
        return back();
    }

    public function addProduct(Request $request,$id)
    {
        // buscar usuario
       $product = Product::findOrFail($id);
       // $categoria = Cathegory::findOrFail($product->cathegory_id);
       // return $categoria->name;

       $basket = $request->session()->get('basket');

       if ( $basket == null){
        $basket = array();
    }

    $position = -1;
    foreach ($basket as $key => $item) {
        if ($item->id == $id) {
            $position = $key;
            break;
        }
    }

    if ($position == -1) {
        $product->cantidad =1;
        $product->importe=$product->price;
        $request->session()->push('basket', $product);
        // return $basket;
    } else {
        // return $product->price*$basket[$position]->cantidad;

        $basket[$position]->cantidad++;
        $basket[$position]->importe=$product->price*$basket[$position]->cantidad;
        // return $basket;
    }
    $total=0;
    foreach ($basket as $verTotal) {
        $total+=$verTotal->importe;

    }
    // $product->total=$total;

    // $request->session()->push('basket', $product);
    // return $basket;
    return redirect("/basket");
}
public function delete(Request $request,$id){//para borrar un elemento de Sesion
    $product=Product::findOrFail($id);
    $basket=$request->session()->get('basket');

    foreach ($basket as $key => $productSesion){
        //productSesion son los usuarios que actualmente estan la group/
        if ($product->id==$basket[$key]->id){
            if ($productSesion->cantidad <=1){
                $request->session()->forget('basket.' .$key);
            }else{
                $productSesion->cantidad--;
            }
            return back();
        }
    }
    return back();
    }//fin delete
    public function store(Request $request)
    {
        $order = New Order;
        $basket = $request->session()->get('basket');

        // return $total;
        $order->paid = 0;
        $order->date = Carbon::today();
        $order->user_id = Auth()->user()->id;

        $order->save();
        // $orderID = Order::all();
        // $orderID->last()->id+1;

        foreach ($basket as $product) {
          $order->products()->attach($product->id,
            [
              'price' => $product->price,
              'quantity' => $product->cantidad,
          ]);
      }
      $orderController = new OrderController;
      $orderController->enviarEmail($order->id);
      return redirect("/basket/flush");

  }

  public static function total()
  {
    $basket= Session::get('basket');
    if ($basket == null ) {
        $basket = array();
        return '';
    }
    $cantidadTotal = 0;
    $precioTotal=0;

    foreach ($basket as $key ) {
        $numProducts=$key->cantidad;
        $cantidadTotal+=$numProducts;
        $precioTotal += $key->price*$key->cantidad;
    }
    return $cantidadTotal." - ".$precioTotal."€";
}

}
