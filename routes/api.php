<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/******************************************/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('products','Api\ProductController@index');//para no mezlcar los controladores html de api se pone /api
Route::get('products/{id}','Api\ProductController@show');//para no mezlcar los controladores html de api se pone /api
Route::post('products','Api\ProductController@store');//para no mezlcar los controladores html de api se pone /api
Route::put('products/{id}','Api\ProductController@update');//para no mezlcar los controladores html de api se pone /api


// para instalar postman
// https://stackoverflow.com/questions/42996358/how-to-install-start-postman-native-v4-10-3-on-ubuntu-16-04-lts-64-bit
