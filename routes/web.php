<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \Carbon\Carbon;

Route::get('/', function () {
    return view('welcome');
});

// Route::get('users', 'UserController@index')->name('usuarios');
// Route::get('users/{id}', 'UserController@show');
// Route::get('users/{id}/edit', 'UserController@edit');
// Route::get('users/create', 'UserController@create');
// Route::put('users/{id}', 'UserController@update');
// Route::delete('users/{id}', 'UserController@destroy');
// Route::post('users', 'UserController@store');
// Route::get('users/create', 'UserController@create');

//Ruta de tipo resource: equivale a las 7 rutas REST
//Ruta especial antes que resource, si no "show...."
Route::get('users/especial', 'UserController@especial');
Route::resource('users', 'UserController');


Route::get('users/{id}/edit2', 'UserController@edit2s');


Route::resource('products', 'ProductController');
Route::resource('category', 'CathegoryController');
Route::resource('roles', 'RoleController');

/**********PARA GUARDAR USUARIOS EN SESSION****************/
Route::get('/group', 'GroupController@index');
Route::get('/group/flush', 'GroupController@flush');//esta linea no puede ser la última

Route::get('/group/{id}', 'GroupController@addUser');



/********* BASKET ***********/

Route::get('basket', 'BasketController@index');
Route::get('basket/flush', 'BasketController@flush');
Route::get('basket/{id}', 'BasketController@addProduct');
Route::post('basket' , 'BasketController@store');

Route::get('basket/delete/{id}', 'BasketController@delete');
Route::get('/hoy',function (){
    $today = Carbon::today();
    return $today->format('d-m-Y');
});

/**************ORDER***************/
Route::get('order', 'OrderController@index');
Route::get('order/{id}', 'OrderController@show');
Route::get('order/{id}/edit', 'OrderController@edit');
Route::put('order/{id}', 'OrderController@update');
Route::delete('order/{id}', 'OrderController@delete');

/**************PDF***************/
Route::get('order/{id}/pdf', 'OrderController@pdf')->name('pedido.pdf');


////////////////////////////////////////////
//Ejemplos de rutas con funciones anónimas:
////////////////////////////////////////////

Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
});


Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
})->where('id', '[0-9]+');

Route::get('usuarios/{id}/{name?}', function ($id, $name=null) {
    if($name) {
        return "Detalle del usuario $id. El nombre es $name";
    } else {
        return "Detalle del usuario $id. Anónimo";
    }
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/////// PARA EL LOGIN CON GOOGLE Y FACEBOOK y github
Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/redirectFacebook', 'Auth\LoginController@redirectToProviderFacebook');
Route::get('/callbackFacebook', 'Auth\LoginController@handleProviderCallbackFacebook');

Route::get('/redirectGithub', 'Auth\LoginController@redirectToProviderGithub');
Route::get('/callbackGithub', 'Auth\LoginController@handleProviderCallbackGithub');

////////EMAIL///////////////
 Route::get('/orders/{id}/enviarEmail', 'OrderController@enviarEmail');
